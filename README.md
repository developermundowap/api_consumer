# Instalação
Siga os passos abaixo para a instalação correta do projeto.

### 1° - Arquivos .env
Gere os arquivos `.env` caso os mesmos não existam e configure com os valores corretos.
```sh
cp config/.env.example config/.env
cp app/conf/.env.example app/conf/.env
```

### 2° - Permissões de Pastas
Dê permissão de escrita e leitura para a pasta `app/logs` (os logs de erro da aplicação serão salvos nesse diretório).
```sh
sudo chmod 775 app/logs
```

### 3° - Composer
Instale as dependências caso já não tenha instalado.
```sh
composer install
```

## Execução
Exemplo de execução do script principal.
```sh
php ./app/index.php ImportData
```