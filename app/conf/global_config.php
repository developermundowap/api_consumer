<?php

use Dotenv\Dotenv;


/*
 * ------------------------------------------------------
 *  Php ini (Errors)
 * ------------------------------------------------------
 */

ini_set('display_errors', false);
error_reporting(E_ALL);
ini_set('log_errors', 1);


/*
 * ------------------------------------------------------
 *  Constants
 * ------------------------------------------------------
 */

define('APP_HOME', dirname(__DIR__));
define('APP_LOGS_DIR', APP_HOME . '/logs');
define('APP_LOG_ERRORS_FILE', APP_LOGS_DIR . '/errors.log');


/*
 * ------------------------------------------------------
 *  Logs
 * ------------------------------------------------------
 */

if (!is_writable(APP_LOGS_DIR))
{
    echo sprintf('Logs directory "%s" is not writable', APP_LOGS_DIR) . PHP_EOL;
    exit;
}


ini_set('error_log', APP_LOG_ERRORS_FILE);


/*
 * ------------------------------------------------------
 *  Error Handler
 * ------------------------------------------------------
 */
set_error_handler(
    function ($code, $message, $file, $line) {

        $reporting = error_reporting();


        // error was suppressed with the @-operator OR error is not reportable
        if ((0 === $reporting) || !($code & $reporting))
        {
            return false;
        }


        throw new ErrorException($message, $code, 1, $file, $line);
    }
);


/*
 * ------------------------------------------------------
 *  Environment
 * ------------------------------------------------------
 */

try
{
    $dotenv = Dotenv::create(__DIR__);
    $dotenv->overload();


    // preenchimento requerido
    $dotenv
        ->required([
            'APP_DB_HOST',
            'APP_DB_USER',
            'APP_DB_PASS',
            'APP_DB_NAME',
            'APP_API_HOST',
            'APP_API_TOKEN',
            'APP_IMPORT_STARTING_DATE',
        ])
        ->notEmpty()
    ;

    // preenchimento requerido (int)
    $dotenv
        ->required([
            'APP_API_RESULTS_LIMIT',
        ])
        ->isInteger()
    ;


    $dotenv->required('APP_ENV')->allowedValues(['development', 'production']);
}
catch (Exception $e)
{
    echo 'Environment vars not configured.' . PHP_EOL;
    exit;
}


/*
 * ------------------------------------------------------
 *  Php ini
 * ------------------------------------------------------
 */

if ($_ENV['APP_ENV'] === 'development')
{
    ini_set('display_errors', true);
    ini_set('log_errors', 0);
}