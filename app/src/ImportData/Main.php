<?php


namespace App\Script\ImportData;


use App\Util\Db\DbHelper;
use App\Util\Script\ScriptMain;
use DateTime;
use League\Pipeline\Pipeline;
use League\Pipeline\PipelineBuilder;
use stdClass;

class Main extends ScriptMain
{
    public const TABLE_IMPORT_CONTROLS = 'import_controls';
    public const TABLE_VW_EXECUCAO = 'vw_execucao';
    public const TABLE_VW_USO = 'vw_uso';
    public const TABLE_VW_LOJAS = 'vw_lojas';
    public const TABLE_VW_PRODUTO = 'vw_produto';


    public function run(array $params): void
    {
        $conn = DbHelper::getConn();
        $dt = new DateTime($_ENV['APP_IMPORT_STARTING_DATE'] . '00:00:00');


        $payload = new stdClass();
        $payload->api_results_limit = (int)$_ENV['APP_API_RESULTS_LIMIT'];
        $payload->imp_starting_date = $dt->format('Y-m-d');
        $payload->tables = [
            self::TABLE_VW_EXECUCAO,
            self::TABLE_VW_USO,
            self::TABLE_VW_LOJAS,
            self::TABLE_VW_PRODUTO,
        ];


        $pipelineBuilder = (new PipelineBuilder)
            ->add(new MigrateTables($this->logger, $conn))
            ->add(new ImportVwUso($this->logger, $conn))
            ->add(new ImportVwExecucao($this->logger, $conn))
            ->add(new ImportVwProduto($this->logger, $conn))
            ->add(new ImportVwLojas($this->logger, $conn));


        /** @var $pipeline Pipeline */
        $pipeline = $pipelineBuilder->build();
        $pipeline->process($payload);
    }
}