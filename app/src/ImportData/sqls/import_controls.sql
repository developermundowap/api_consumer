SET NAMES utf8mb4;

-- ----------------------------
-- Table structure for import_controls
-- ----------------------------

CREATE TABLE IF NOT EXISTS `import_controls` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table` enum('vw_execucao','vw_uso','vw_lojas','vw_produto') NOT NULL,
  `last_import_val` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `table` (`table`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
