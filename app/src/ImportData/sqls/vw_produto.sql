SET NAMES utf8mb4;

-- ----------------------------
-- Table structure for vw_produto
-- ----------------------------

CREATE TABLE IF NOT EXISTS `vw_produto` (
  `id` int(11) NOT NULL,
  `Codigo` varchar(30),
  `EAN` varchar(255),
  `Produto` varchar(255),
  `Categoria` varchar(80),
  `SubCategoria` varchar(80),
  `Fabricante` varchar(7),
  `Marca` varchar(80),
  `Linha` char(0),
  `PrecoMinimoAtacado` decimal(8,2),
  `PrecoMaximoAtacado` decimal(8,2),
  `PrecoSugeridoAtacado` char(0),
  `PrecoMinimoVarejo` char(0),
  `PrecoMaximoVarejo` char(0),
  `PrecoSugeridoVarejo` char(0),
  `Ativo` varchar(10),
  `Produto_id` int(11),
  `Categoria_id` int(11),
  `SubCategoria_id` varchar(11),
  `Fabricante_id` varchar(11),
  `Linha_id` char(0),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;