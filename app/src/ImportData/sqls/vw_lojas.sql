SET NAMES utf8mb4;

-- ----------------------------
-- Table structure for vw_lojas
-- ----------------------------

CREATE TABLE IF NOT EXISTS `vw_lojas` (
  `id` int(6) unsigned zerofill NOT NULL,
  `matriculaloja` int(6) unsigned zerofill,
  `cnpj` varchar(106),
  `inscest` varchar(20),
  `razaosocial` varchar(80),
  `nomefantasia` varchar(80),
  `cep` varchar(9),
  `tp_logradouro` varchar(20),
  `logradouro` varchar(150),
  `numero` varchar(10),
  `complemento` varchar(20),
  `bairro` varchar(80),
  `cidade` varchar(80),
  `estado` varchar(50),
  `raio` int(6),
  `email` varchar(80),
  `comercial1` varchar(15),
  `comercial2` varchar(15),
  `comercial3` varchar(15),
  `contato` varchar(80),
  `niverLoja` date,
  `codLoja` varchar(25),
  `grupo` varchar(80),
  `rede` varchar(80),
  `bandeira` varchar(80),
  `tipologia` varchar(250),
  `classificacao` varchar(250),
  `numCheck` varchar(250),
  `pdv` varchar(150),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
