<?php


namespace App\Script\ImportData;


use App\Util\Db\DbHelper;

class ImportVwLojas extends ImportVw
{
    protected function _getTable(): string
    {
        return Main::TABLE_VW_LOJAS;
    }

    protected function _getPages(string $lastImpVal): int
    {
        $options = $this->getOptions($lastImpVal);
        $pages = $this->api->getStoresPages($options);


        return $pages;
    }

    protected function _getRows(int $page, string $lastImpVal): array
    {
        $options = $this->getOptions($lastImpVal);
        $options['query']['page'] = $page;
        $stores = $this->api->getStores($options);
        $rows = [];


        foreach ($stores as $i => $store)
        {
            $storeId = $store["id"];


            $details = $this->api->getStore($storeId, false);
            $market = $this->api->getMarket($details["market_id"]);
            $chain = $this->api->getMarket($details["market_chain_id"]);
            $flag = $this->api->getMarket($details["market_flag_id"]);


            $row = [
                'id'            => $storeId,
                'matriculaloja' => $storeId,
                'cnpj'          => $details["document"],
                'inscest'       => $details["inscest"],
                'razaosocial'   => $details["company_name"],
                'nomefantasia'  => $details["name"],
                'cep'           => $details["postal_code"],
                'tp_logradouro' => $details["street_type"],
                'logradouro'    => $details["street_address"],
                'numero'        => $details["street_number"],
                'complemento'   => $details["complement"],
                'bairro'        => $details["sublocality"],
                'cidade'        => $details["city"],
                'estado'        => $details["state"],
                'raio'          => $details["radius"],
                'email'         => $details["email"],
                'comercial1'    => $details["phone1"],
                'comercial2'    => $details["phone2"],
                'comercial3'    => $details["phone3"],
                'contato'       => $details["contact"],
                'niverLoja'     => $details["birthday"],
                'codLoja'       => $details["code"],
                'grupo'         => $market['name'] ?? '',
                'rede'          => $chain['name'] ?? '',
                'bandeira'      => $flag['name'] ?? '',
                'tipologia'     => $details["typology"],
                'classificacao' => $details["classification"],
                'numCheck'      => $details["checkouts"],
                'pdv'           => $details["pdv"],
            ];


            $rows[] = $row;
        }


        return $rows;
    }

    protected function _saveRows(array $rows): string
    {
        $lastImpVal = $this->getDtNow()->format('Y-m-d H:i:s');

        DbHelper::insert($this->conn, $this->_getTable(), $rows);


        return $lastImpVal;
    }

    private function getOptions(string $lastImpVal): array
    {
        $opt = [
            'query' => [
                'limit'     => $this->p->api_results_limit,
                'sort'      => 'id',
                'direction' => 'asc',
            ]
        ];


        if ($lastImpVal)
        {
            $opt['query']['last_modified'] = $lastImpVal;
        }


        return $opt;
    }
}