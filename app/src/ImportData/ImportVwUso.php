<?php


namespace App\Script\ImportData;


use App\Util\Db\DbHelper;

class ImportVwUso extends ImportVw
{
    protected function _getTable(): string
    {
        return Main::TABLE_VW_USO;
    }

    protected function _getPages(string $lastImpVal): int
    {
        $options = $this->getOptions($lastImpVal);
        $pages = $this->api->getAttendancesPages($options);


        return $pages;
    }

    protected function _getRows(int $page, string $lastImpVal): array
    {
        $options = $this->getOptions($lastImpVal);
        $options['query']['page'] = $page;
        $attendances = $this->api->getAttendances($options);
        $rows = [];


        foreach ($attendances as $i => $att)
        {
            $duration = $this->sec2hours($att["duration"]);
            $market = $this->api->getMarket($att["store"]["market_id"]);
            $chain = $this->api->getMarket($att["store"]["market_chain_id"]);
            $flag = $this->api->getMarket($att["store"]["market_flag_id"]);


            $row = [
                'id'                       => $att['id'],
                'Campanha'                 => 'Promotor',
                'CPF_Colaborador'          => $att["people"]["document"],
                'Colaborador'              => $att["people"]["name"],
                'Telefone_Colaborador'     => $att["people"]["phone"],
                'CPF_Superior'             => $att["people"]["supervisor"]["document"] ?? null,
                'Superior'                 => $att["people"]["supervisor"]["name"] ?? null,
                'Data_da_Visita'           => $att["date"],
                'Origem'                   => ($att["check_in_origin"] === 'C') ? 'Mobile' : 'Web',
                'Hora_Check_In'            => $att["check_in"],
                'Coordenada_Check_In'      => $att["check_in_lat"] . ',' . $att["check_in_lng"],
                'Distancia_Check_In'       => $att["check_in_distance"],
                'Check_In_Dentro_do_Raio'  => ($att["check_in_type"] === 'NA') ? 'VERDADEIRO' : 'FALSO',
                'Photo_Check_In'           => $att["check_in_photo"],
                'Hora_Check_Out'           => $att["check_out"],
                'Coordenada_Check_Out'     => $att["check_out_lat"] . ',' . $att["check_out_lng"],
                'Distancia_Check_Out'      => $att["check_out_distance"],
                'Check_Out_Dentro_do_Raio' => ($att["check_out_type"] === 'NA') ? 'VERDADEIRO' : 'FALSO',
                'Photo_Check_Out'          => $att["check_out_photo"],
                'Check_Out_Automatico'     => ($att["check_out_type"] === 'A') ? 'VERDADEIRO' : 'FALSO',
                'Permanencia'              => $duration,
                'Deslocamento'             => $att["distance_travel"],
                'Intervalos_PDV'           => '00:00:00',
                'Intervalos_Deslocamento'  => '00:00:00',
                'Forms_Respondidos'        => count($att["forms"]),
                'Forms_Totais'             => null,
                'Rede'                     => $chain["name"] ?? "",
                'Bandeira'                 => $flag["name"] ?? "",
                'PDV'                      => $market["name"] ?? "",
                'PDV_Validado'             => ($att["store"]["approved"] === 'S') ? 'VERDADEIRO' : 'FALSO',
                'Codigo_PDV'               => $att["store"]["code"],
                'CNPJ'                     => $att["store"]["document"],
                'Endereco'                 => $att["store"]["street_type"] . ' ' . $att["store"]["street_address"] . ', ' . $att["store"]["street_number"] . ($att["store"]["complement"] ? ' - ' . $att["store"]["complement"] : ''),
                'Bairro'                   => $att["store"]["sublocality"],
                'Cidade'                   => $att["store"]["city"],
                'UF'                       => $att["store"]["state"],
                'Area_Nielsen'             => null,
                'Quantidade_Checkouts'     => $att["store"]["checkouts"],
                'Codigo_Varejo'            => $att["store"]["code"],
                'Tipo_Documento'           => 'CNPJ',
                'Bateria_Check_In'         => $att["batery_in"],
                'Bateria_Check_Out'        => $att["batery_out"],
                'Distancia_Anterior'       => $att["distance_travel"],
                'Distancia_Casa'           => $att["distance_home"],
                'Colaborador_id'           => $att["people"]["id"],
                'Superior_id'              => $att["people"]["supervisor"]["id"] ?? null,
                'PDV_id'                   => $att["store"]["id"],
                'Rede_id'                  => $att["store"]["market_chain_id"],
                'Bandeira_id'              => $att["store"]["id"],
                'Canal_id'                 => $att["store"]["market_id"],
            ];


            $rows[] = $row;
        }


        return $rows;
    }

    protected function _saveRows(array $rows): string
    {
        $lastImpVal = '';


        foreach ($rows as $row)
        {
            $lastImpVal = $row['id'];
        }


        DbHelper::insert($this->conn, $this->_getTable(), $rows);


        return $lastImpVal;
    }

    protected function getOptions(string $lastImpVal): array
    {
        $opt = [
            'query' => [
                'limit'     => $this->p->api_results_limit,
                'sort'      => 'id',
                'direction' => 'asc',
            ]
        ];


        if ($lastImpVal)
        {
            $opt['query']['last_id'] = $lastImpVal;
        }
        else
        {
            $opt['query']['date'] = $this->p->imp_starting_date;
        }


        return $opt;
    }
}