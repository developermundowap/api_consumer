<?php


namespace App\Script\ImportData;


use App\Util\Db\DbHelper;

class ImportVwProduto extends ImportVw
{
    protected function _getTable(): string
    {
        return Main::TABLE_VW_PRODUTO;
    }

    protected function _getPages(string $lastImpVal): int
    {
        $options = $this->getOptions($lastImpVal);
        $pages = $this->api->getProductsPages($options);


        return $pages;
    }

    protected function _getRows(int $page, string $lastImpVal): array
    {
        $options = $this->getOptions($lastImpVal);
        $options['query']['page'] = $page;
        $products = $this->api->getProducts($options);


        $rows = [];
        foreach ($products as $i => $prod)
        {
            $productId = $prod['id'];


            $details = $this->api->getProduct($productId);
            $category = $details["categories"][0]['name'] ?? '';
            $subcategoryId = $details["categories"][1]['id'] ?? '';
            $subcategory = $details["categories"][1]['name'] ?? '';
            $supplier = $details["supplier"]["name"] ?? '';
            $brand = $details["brand"]["name"] ?? '';


            if (!$supplier && $details['type'] == 1)
            {
                $supplier = 'BOM DIA';
            }


            $row = [
                'id'                   => $productId,
                'Codigo'               => $details["code"],
                'EAN'                  => $details["barcode"],
                'Produto'              => $details["name"],
                'Categoria'            => $category,
                'SubCategoria'         => $subcategory,
                'Fabricante'           => $supplier,
                'Marca'                => $brand,
                'Linha'                => null,
                'PrecoMinimoAtacado'   => $details["price_min"],
                'PrecoMaximoAtacado'   => $details["price_max"],
                'PrecoSugeridoAtacado' => null,
                'PrecoMinimoVarejo'    => null,
                'PrecoMaximoVarejo'    => null,
                'PrecoSugeridoVarejo'  => null,
                'Ativo'                => $details["status"] ? 'VERDADEIRO' : 'FALSO',
                'Produto_id'           => $productId,
                'Categoria_id'         => $details["category_id"],
                'SubCategoria_id'      => $subcategoryId,
                'Fabricante_id'        => $details["supplier_id"],
                'Linha_id'             => null,
            ];


            $rows[] = $row;
        }


        return $rows;
    }

    protected function _saveRows(array $rows): string
    {
        $lastImpVal = $this->getDtNow()->format('Y-m-d H:i:s');

        DbHelper::insert($this->conn, $this->_getTable(), $rows);


        return $lastImpVal;
    }

    private function getOptions(string $lastImpVal): array
    {
        $opt = [
            'query' => [
                'limit'     => $this->p->api_results_limit,
                'sort'      => 'id',
                'direction' => 'asc',
            ]
        ];


        if ($lastImpVal)
        {
            $opt['query']['last_modified'] = $lastImpVal;
        }


        return $opt;
    }
}