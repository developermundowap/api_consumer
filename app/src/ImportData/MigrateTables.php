<?php


namespace App\Script\ImportData;


use App\Util\Script\ScriptHandlerDb;

class MigrateTables extends ScriptHandlerDb
{
    protected function _describe(): string
    {
        return 'Migratting tables';
    }

    protected function _exec(): void
    {
        $this->createTableIfNotExists(Main::TABLE_IMPORT_CONTROLS);


        foreach ($this->p->tables as $table)
        {
            $this->createTableIfNotExists($table);
        }
    }

    private function createTableIfNotExists(string $table): void
    {
        $this->logger->info($table);


        $sqlPath = sprintf('%s/sqls/%s.sql', __DIR__, $table);
        $sql = file_get_contents($sqlPath);


        $this->conn->exec($sql);
    }
}