<?php


namespace App\Script\ImportData;


use App\Util\Api\ApiClient;
use App\Util\Script\ScriptHandlerDb;
use DateTime;
use DateTimeZone;
use Monolog\Logger;
use PDO;

abstract class ImportVw extends ScriptHandlerDb
{
    /** @var ApiClient */
    protected $api;


    public function __construct(Logger $logger, PDO $conn)
    {
        parent::__construct($logger, $conn);

        $this->api = new ApiClient();
    }

    protected function _describe(): string
    {
        return 'importing data from ' . $this->_getTable();
    }

    protected function _exec(): void
    {
        $lastImpVal = $this->getLastImportVal();
        $lastImpValCurrent = '';
        $pages = $this->_getPages($lastImpVal);


        for ($page = 1; $page <= $pages; $page++)
        {
            $this->logger->info(sprintf('page %d/%d', $page, $pages));


            $rows = $this->_getRows($page, $lastImpVal);
            $lastImpValCurrent = $this->_saveRows($rows);
        }


        $this->saveLastImportVal($lastImpValCurrent);
    }

    protected function sec2hours(int $sec): string
    {
        $h = floor($sec / 60 / 60);
        $m = floor($sec / 60);
        $s = $sec % 60;


        $hours = sprintf('%02d:%02d:%02d', $h, $m, $s);


        return $hours;
    }

    protected function getDtNow(string $timezone = 'America/Sao_Paulo'): DateTime
    {
        $tz = new DateTimeZone($timezone);
        $dt = new DateTime('now', $tz);


        return $dt;
    }

    protected abstract function _getTable(): string;

    protected abstract function _getPages(string $lastImpVal): int;

    protected abstract function _getRows(int $page, string $lastImpVal): array;

    protected abstract function _saveRows(array $rows): string;

    private function getLastImportVal(): string
    {
        $table = Main::TABLE_IMPORT_CONTROLS;
        $pTable = $this->conn->quote($this->_getTable());


        $sql = "
            SELECT
                last_import_val
            FROM {$table}
            WHERE
                `table`={$pTable}
        ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();


        $ret = $row['last_import_val'] ?? '';


        return $ret;
    }

    private function saveLastImportVal(string $lastImpVal): void
    {
        if (!$lastImpVal)
        {
            return;
        }


        $table = Main::TABLE_IMPORT_CONTROLS;
        $pTable = $this->conn->quote($this->_getTable());
        $pVal = $this->conn->quote($lastImpVal);


        $sql = "
            INSERT INTO {$table}
            SET
                `table`={$pTable} 
                ,last_import_val={$pVal}
            ON DUPLICATE KEY UPDATE
                last_import_val={$pVal}
        ";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }
}