<?php


namespace App\Script\ImportData;


use App\Util\Db\DbHelper;
use DateTime;
use RuntimeException;

class ImportVwExecucao extends ImportVw
{
    protected function _getTable(): string
    {
        return Main::TABLE_VW_EXECUCAO;
    }

    protected function _getPages(string $lastImpVal): int
    {
        $options = $this->getOptions($lastImpVal);
        $pages = $this->api->getRegistriesPages($options);


        return $pages;
    }

    protected function _getRows(int $page, string $lastImpVal): array
    {
        $options = $this->getOptions($lastImpVal);
        $options['query']['page'] = $page;
        $regsGrouped = $this->api->getRegistries($options);
        $rows = [];


        $attendances = array_column($regsGrouped['data'], 'attendance_id');
        $attendances = array_filter($attendances);


        foreach ($attendances as $attId)
        {
            $att = $this->api->getAttendance($attId);


            $attendanceId = $att['id'];
            $duration = $this->sec2hours($att["duration"]);
            $market = $this->api->getMarket($att["store"]["market_id"]);
            $chain = $this->api->getMarket($att["store"]["market_chain_id"]);
            $flag = $this->api->getMarket($att["store"]["market_flag_id"]);
            $registries = $this->getRegistries($attendanceId);


            $baseRow = [
                'id'                             => null,
                'Campanha'                       => 'Promotor',
                'CPF_Colaborador'                => $att["people"]["document"],
                'Colaborador'                    => $att["people"]["name"],
                'Perfil'                         => $att["people"]["profiles"][0]['name'] ?? null,
                'Superior'                       => $att["people"]["supervisor"]["name"] ?? null,
                'Data_da_Visita'                 => $att["date"],
                'Origem'                         => ($att["check_in_origin"] === 'C') ? 'Mobile' : 'Web',
                'Hora_Check_In'                  => $att["check_in"],
                'Latitude_Check_In'              => $att["check_in_lat"],
                'Longitude_Check_In'             => $att["check_in_lng"],
                'Distancia_Check_In'             => $att["check_in_distance"],
                'Check_In_Dentro_do_Raio'        => ($att["check_in_type"] === 'NA') ? 'VERDADEIRO' : 'FALSO',
                'Hora_Check_Out'                 => $att["check_out"],
                'Latitude_Check_Out'             => $att["check_out_lat"],
                'Longitude_Check_Out'            => $att["check_out_lng"],
                'Distancia_Check_Out'            => $att["check_out_distance"],
                'Check_Out_Dentro_do_Raio'       => ($att["check_out_type"] === 'NA') ? 'VERDADEIRO' : 'FALSO',
                'Check_Out_Automatico'           => ($att["check_out_type"] === 'A') ? 'VERDADEIRO' : 'FALSO',
                'Permanencia'                    => $duration,
                'Deslocamento'                   => $att["distance_travel"],
                'Intervalos_PDV'                 => '00:00:00',
                'Intervalos_Deslocamento'        => '00:00:00',
                'PDV'                            => $att["store"]["pdv"],
                'PDV_Validado'                   => ($att["store"]["approved"] === 'S') ? 'VERDADEIRO' : 'FALSO',
                'Codigo_PDV'                     => $att["store"]["code"],
                'CNPJ'                           => $att["store"]["document"],
                'Canal'                          => $market["name"] ?? '',
                'Rede'                           => $chain["name"] ?? '',
                'Bandeira'                       => $flag["name"] ?? '',
                'Area_Nielsen'                   => null,
                'Numero_de_Checkouts'            => $att["store"]["checkouts"],
                'Codigo_do_Varejo'               => $att["store"]["code"],
                'CEP'                            => $att["store"]["postal_code"],
                'Tipo_Logradouro'                => $att["store"]["street_type"],
                'Logradouro'                     => $att["store"]["street_address"],
                'Numero'                         => $att["store"]["street_number"],
                'Bairro'                         => $att["store"]["sublocality"],
                'Cidade'                         => $att["store"]["city"],
                'Estado'                         => $att["store"]["state"],
                'Tipo_Formulario'                => 'FORMULARIOS',
                'Formulario'                     => null,
                'Data_do_Formulario'             => null,
                'Ordem_da_Pergunta'              => null,
                'Pergunta'                       => null,
                'Resposta'                       => null,
                'Resposta_Numerica'              => null,
                'Data_da_Resposta'               => null,
                'Hora_da_Resposta'               => null,
                'Semana_do_Ano'                  => null,
                'Mes'                            => null,
                'Ano'                            => null,
                'Pontuacao_da_Resposta'          => 0,
                'Pontuacao_Absoluta_da_Pergunta' => 0,
                'Pontuacao_Relativa_da_Pergunta' => 0,
                'Pontuacao_Calculada'            => 0,
                'Tipo_Documento'                 => 'CNPJ',
                'Produto'                        => null,
                'EAN'                            => null,
                'Categoria'                      => null,
                'Fabricante'                     => null,
                'Rotulo'                         => null,
                'Colaborador_id'                 => $att["people"]["id"],
                'Superior_id'                    => $att["people"]["supervisor"]["id"] ?? null,
                'PDV_id'                         => $att["store"]["id"],
                'Rede_id'                        => $att["store"]["market_chain_id"],
                'Bandeira_id'                    => $att["store"]["id"],
                'Canal_id'                       => $att["store"]["market_id"],
                'Produto_id'                     => null,
            ];


            foreach ($registries as $reg)
            {
                $dt = new DateTime($reg["mobile_date"]);


                $row = $baseRow;
                $row['Formulario'] = $reg["form"]["name"];
                $row['Data_do_Formulario'] = $dt->format('Y-m-d');
                $row['Data_da_Resposta'] = $dt->format('Y-m-d');
                $row['Hora_da_Resposta'] = $dt->format('H:i:s');
                $row['Semana_do_Ano'] = $dt->format('W');
                $row['Mes'] = $dt->format('m');
                $row['Ano'] = $dt->format('Y');


                $labels = array_column($reg['form']['fields'], 'label', 'id');


                foreach ($reg["values"] as $i => $value)
                {
                    $valueId = $value['id'];
                    $productId = $value["product_id"];
                    $fieldId = $value['field_id'];
                    $fieldVal = $value["field_value"];
                    $ordem = $i + 1;


                    if ($product = $this->api->getProduct($productId))
                    {
                        $row['Produto'] = $product['name'];
                        $row['EAN'] = $product["barcode"];
                        $row['Categoria'] = $product["categories"][0]["name"] ?? '';
                        $row['Fabricante'] = $product["supplier"]["name"] ?? '';
                        $row['Produto_id'] = $productId;
                    }


                    $row['id'] = $valueId;
                    $row['Ordem_da_Pergunta'] = $ordem;
                    $row['Pergunta'] = $labels[$fieldId];
                    $row['Resposta'] = !is_numeric($fieldVal) ? $fieldVal : '';
                    $row['Resposta_Numerica'] = is_numeric($fieldVal) ? $fieldVal : '';


                    $rows[] = $row;
                }
            }
        }


        return $rows;
    }

    protected function _saveRows(array $rows): string
    {
        $lastImpVal = date('Y-m-d H:i:s');


        DbHelper::insert($this->conn, $this->_getTable(), $rows);


        return $lastImpVal;
    }

    private function getOptions(string $lastImpVal): array
    {
        $opt = [
            'query' => [
                'limit'           => $this->p->api_results_limit,
                'sort'            => 'id',
                'direction'       => 'asc',
                'grouped'         => 'attendance_id',
                'have_attendance' => 1,
            ]
        ];


        $lastModified = ($lastImpVal) ? $lastImpVal : $this->p->imp_starting_date . ' 00:00:00';


        $opt['query']['last_modified'] = $lastModified;


        return $opt;
    }

    private function getRegistries(int $attendanceId): array
    {
        $page = 1;


        do
        {
            $json = $this->api->getRegistries([
                'query' => [
                    'attendance_id' => $attendanceId,
                    'contain'       => 'Values',
                    'page'          => $page,
                ]
            ]);
            $next = $json["pagination"]["has_next_page"];
            $page++;
            $registries = $json["data"];


            foreach ($registries as $i => $reg)
            {
                $formId = $reg["form_id"];


                if (!($form = $this->api->getForm($formId)))
                {
                    throw new RuntimeException(sprintf('form "%d" not found.', $formId));
                }


                $reg['form'] = $form;


                $registries[$i] = $reg;
            }


        } while ($next);


        return $registries;
    }
}