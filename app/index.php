<?php


use App\Util\Script\ScriptMain;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;


require 'vendor/autoload.php';


/*
 * ------------------------------------------------------
 *  Args
 * ------------------------------------------------------
 */

$channel = $argv[1];
$params = [];


foreach (array_slice($argv, 2) as $param)
{
    $param = explode('=', $param);
    $k = array_shift($param);
    $v = implode('=', $param); // caso o valor do parâmetro contenha o sinal de '='


    $params[$k] = $v;
}


/*
 * ------------------------------------------------------
 *  Constants
 * ------------------------------------------------------
 */

define('APP_SCRIPT_CHANNEL', $channel);


/*
 * ------------------------------------------------------
 *  Logger
 * ------------------------------------------------------
 */
$lineFormatter = new LineFormatter('[%datetime%|%level_name%] %message%' . PHP_EOL, 'H:i:s');


$handler = new StreamHandler('php://stdout');
$handler->setFormatter($lineFormatter);


$handlerErr = new StreamHandler('php://stderr', Logger::NOTICE, false);
$handlerErr->setFormatter($lineFormatter);


$logger = new Logger(APP_SCRIPT_CHANNEL);
$logger->pushHandler($handler);
$logger->pushHandler($handlerErr);


/*
 * ------------------------------------------------------
 *  Script
 * ------------------------------------------------------
 */

$class = sprintf('App\\Script\\%s\\Main', APP_SCRIPT_CHANNEL);


if (!class_exists($class))
{
    $logger->error(sprintf('channel "%s" not found.', APP_SCRIPT_CHANNEL));
    die(1);
}


try
{
    /** @var $script ScriptMain */
    $script = new $class($logger);
    $script->run($params);
}
catch (Throwable $e)
{
    $logger->error('An unexpected error has occurred.');

    throw $e;
}