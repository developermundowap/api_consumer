<?php


namespace App\Util\Db;


use PDO;

abstract class DbHelper
{
    public static function getConn(): PDO
    {
        $host = $_ENV['APP_DB_HOST'];
        $user = $_ENV['APP_DB_USER'];
        $pass = $_ENV['APP_DB_PASS'];
        $name = $_ENV['APP_DB_NAME'];
        $dsn = sprintf('mysql:host=%s;dbname=%s', $host, $name);


        $conn = new PDO($dsn, $user, $pass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);


        return $conn;
    }

    public static function insert(PDO $conn, string $table, array $rows): int
    {
        if (!$rows)
        {
            return 0;
        }


        $cols = [];
        $colsScaped = [];
        $valuesInsert = [];
        $onDuplicate = [];


        // first row
        foreach ($rows[0] as $col => $val)
        {
            $colScp = sprintf('`%s`', $col);


            $cols[] = $col;
            $colsScaped[] = $colScp;
            $onDuplicate[] = sprintf('%s=VALUES(%s)', $colScp, $colScp);
        }


        // all rows
        foreach ($rows as $row)
        {
            $values = [];


            foreach ($cols as $col)
            {
                $val = $row[$col];
                $val = is_null($val) ? 'NULL' : $conn->quote($val);

                $values[$col] = $val;
            }


            $valuesInsert[] = '(' . implode(',', $values) . ')';
        }


        $colsInsert = '(' . implode(',', $colsScaped) . ')';
        $valuesInsert = implode("\n" . ',', $valuesInsert);
        $onDuplicate = implode("\n" . ',', $onDuplicate);
        $table = sprintf('`%s`', $table);


        $sql = <<<SQL
INSERT INTO {$table}
{$colsInsert}
VALUES
{$valuesInsert}
ON DUPLICATE KEY UPDATE
{$onDuplicate}
SQL;
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $affected = $stmt->rowCount();


        return $affected;
    }
}