<?php


namespace App\Util\Api;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use RuntimeException;
use Throwable;


/**
 * Class ApiConsumer
 */
class ApiClient
{
    /** @var array */
    private $cache = [
        'forms',
        'markets',
        'peoples',
        'products',
        'stores',
    ];

    /** @var Client */
    private $client;

    /**
     * ApiConsumer constructor.
     */
    public function __construct()
    {
        $host = $_ENV['APP_API_HOST'];
        $token = $_ENV['APP_API_TOKEN'];


        $this->client = new Client([
            'base_uri' => $host,
            'headers'  => [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ]
        ]);
    }

    /**
     * Obter lista de atendimentos
     *
     * @param array $options
     * @return array
     * @throws Exception|GuzzleException
     */
    public function getAttendances(array $options = []): array
    {
        $rows = $this->get('/v1/attendances', $options);


        // get rows
        foreach ($rows as $i => $attendance)
        {
            $rows[$i] = $this->appendAttendance($attendance);
        }


        return $rows;
    }

    public function getAttendance(int $id): array
    {
        $attendance = $this->get("/v1/attendances/{$id}");
        $attendance = $this->appendAttendance($attendance);

        return $attendance;
    }

    public function getAttendancesPages(array $options = []): int
    {
        $json = $this->get('/v1/attendances', $options, false);

        if (!$json["pagination"]["count"])
        {
            return 0;
        }

        $pages = $json["pagination"]["page_count"];


        return $pages;
    }

    public function getRegistries(array $options = []): array
    {
        $rows = $this->get('/v1/registries', $options, false);

        return $rows;
    }

    public function getRegistriesPages(array $options = []): int
    {
        $json = $this->get('/v1/registries', $options, false);

        if (!$json["pagination"]["count"])
        {
            return 0;
        }

        $pages = $json["pagination"]["page_count"];


        return $pages;
    }

    public function getStores(array $options = []): array
    {
        $rows = $this->get('v1/stores', $options);

        return $rows;
    }

    public function getStoresPages(array $options = []): int
    {
        $json = $this->get('v1/stores', $options, false);

        if (!$json["pagination"]["count"])
        {
            return 0;
        }

        $pages = $json["pagination"]["page_count"];

        return $pages;
    }

    public function getProducts(array $options = []): array
    {
        $rows = $this->get('v1/products', $options);

        return $rows;
    }

    public function getProductsPages(array $options = []): int
    {
        $json = $this->get('v1/products', $options, false);

        if (!$json["pagination"]["count"])
        {
            return 0;
        }

        $pages = $json["pagination"]["page_count"];

        return $pages;
    }

    /**
     * Obter dados de uma loja
     *
     * @param int $store_id
     * @param bool $cache
     * @return array
     */
    public function getStore(int $store_id, bool $cache = true): array
    {
        try
        {
            if ($cache)
            {
                if (isset($this->cache['stores'][$store_id]))
                {
                    $entity = $this->cache['stores'][$store_id];
                }
                else
                {
                    // get result
                    $entity = $this->get('v1/stores/' . $store_id);

                    // set cache
                    $this->cache['stores'][$store_id] = $entity;
                }
            }
            else
            {
                // get result
                $entity = $this->get('v1/stores/' . $store_id);
            }
        }
        catch (Throwable $e)
        {
            $entity = [];
        }

        return $entity;
    }

    /**
     * Obter dados de produto
     *
     * @param int|null $product_id
     * @return array
     */
    public function getProduct(int $product_id = null): array
    {
        try
        {
            if (!$product_id)
            {
                return [];
            }


            if (isset($this->cache['products'][$product_id]))
            {
                $entity = $this->cache['products'][$product_id];
            }
            else
            {
                // get result
                $entity = $this->get('v1/products/' . $product_id);

                // set cache
                $this->cache['products'][$product_id] = $entity;
            }
        }
        catch (Throwable $e)
        {
            $entity = [];
        }

        return $entity;
    }

    /**
     * Obter dados de formulário
     *
     * @param int $form_id
     * @return array
     * @throws Exception
     */
    public function getForm(int $form_id): array
    {
        try
        {
            if (isset($this->cache['forms'][$form_id]))
            {
                $entity = $this->cache['forms'][$form_id];
            }
            else
            {
                // get result
                $entity = $this->get('v1/forms/' . $form_id);

                // set cache
                $this->cache['forms'][$form_id] = $entity;
            }
        }
        catch (Throwable $e)
        {
            $entity = [];
        }

        return $entity;
    }

    /**
     * Obter dados de mercado / rede / bandeira
     *
     * @param int|null $id
     * @return array
     */
    public function getMarket(int $id = null): array
    {
        if (!$id)
        {
            return [];
        }


        try
        {
            if (isset($this->cache['markets'][$id]))
            {
                $entity = $this->cache['markets'][$id];
            }
            else
            {
                // get result
                $entity = $this->get('v1/markets/' . $id);

                // set cache
                $this->cache['markets'][$id] = $entity;
            }
        }
        catch (Throwable $e)
        {
            $entity = [];
        }

        return $entity;
    }

    /**
     * @param string $uri
     * @param array $options
     * @param bool $onlyData
     * @return mixed
     * @throws GuzzleException
     */
    private function get(string $uri, array $options = [], bool $onlyData = true)
    {
        $res = $this->client->get($uri, $options);
        $body = $res->getBody();
        $data = json_decode($body, true);


        if (!$data['success'])
            throw new RuntimeException(sprintf('Unable to get data from the API (uri: %s)', $uri));


        $ret = ($onlyData) ? $data['data'] : $data;


        return $ret;
    }

    /**
     * Obter dados de uma pessoa
     *
     * @param int $people_id
     * @return array
     * @throws Exception
     */
    private function getPeople(int $people_id): array
    {
        try
        {
            if (isset($this->cache['peoples'][$people_id]))
            {
                $entity = $this->cache['peoples'][$people_id];
            }
            else
            {
                // get result
                $entity = $this->get('v1/peoples/' . $people_id, [
                    'query' => [
                        'contain' => 'Supervisor',
                    ]
                ]);

                // set cache
                $this->cache['peoples'][$people_id] = $entity;
            }

        }
        catch (Throwable $e)
        {
            throw new Exception('unable to get attendances');
        }

        return $entity;
    }

    private function appendAttendance(array $attendance): array
    {
        $attendance['people'] = $this->getPeople($attendance['people_id']);
        $attendance['store'] = $this->getStore($attendance['store_id']);


        $attendance['forms'] = $this->get('/v1/registries', [
            'query' => [
                'attendance_id' => $attendance['id'],
                'grouped'       => 'form_id',
            ]
        ]);


        return $attendance;
    }
}