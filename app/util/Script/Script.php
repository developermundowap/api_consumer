<?php


namespace App\Util\Script;


use Monolog\Logger;

abstract class Script
{
    /** @var Logger */
    protected $logger;


    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }
}