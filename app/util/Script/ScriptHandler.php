<?php


namespace App\Util\Script;


use stdClass;

abstract class ScriptHandler extends Script
{
    /** @var stdClass */
    protected $p;


    public function __invoke(stdClass $p): stdClass
    {
        $this->p = $p;


        $this->logger->info('=================================================================================');
        $this->logger->info($this->_describe());


        $this->_exec();


        $this->logger->info('OK');


        return $this->p;
    }

    protected abstract function _describe(): string;

    protected abstract function _exec(): void;
}