<?php


namespace App\Util\Script;


use Monolog\Logger;
use PDO;

abstract class ScriptHandlerDb extends ScriptHandler
{
    /** @var PDO */
    protected $conn;


    public function __construct(Logger $logger, PDO $conn)
    {
        parent::__construct($logger);

        $this->conn = $conn;
    }
}