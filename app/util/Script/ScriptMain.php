<?php


namespace App\Util\Script;


abstract class ScriptMain extends Script
{
    public abstract function run(array $params): void;
}